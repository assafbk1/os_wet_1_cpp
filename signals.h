#ifndef _SIGS_H
#define _SIGS_H
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <iostream>
#include "smash_class.h"

extern Smash smash;

void sigint_handler(int sig_num);
void sigtstp_handler(int sig_num);


#endif

