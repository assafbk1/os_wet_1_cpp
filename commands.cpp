//		commands.c
//********************************************
#include <sys/stat.h>
#include "commands.h"
#define MAX_LINE_SIZE 80
#define MAX_PATH_SIZE 255
#define HIST_ARRAY_SIZE 50
char l_pwd[MAX_PATH_SIZE]; //contain the last pwd

using std::cout;
using std::endl;
char signal_names[32][10] ={ "SIGHUP", "SIGINT", "SIGQUIT", "SIGILL","SIGTRAP","SIGIOT","SIGBUS","SIGFPE","SIGKILL","SIGUSR1","SIGSEGV","SIGUSR2"
                     ,"SIGPIPE", "SIGALRM","SIGTERM","SIGSTKFLT","SIGCHLD","SIGCONT","SIGSTOP", "SIGTSTP","SIGTTIN","SIGTTOU","SIGURG","SIGXCPU","SIGXFSZ","SIGVTALRM" ,
                     "SIGPROF", "SIGWINCH" ,"SIGIO","SIGPWR","SIGSYS"};
//********************************************
// function name: ExeCmd
// Description: interperts and executes built-in commands
// Parameters: pointer to smash, command string
// Returns: 0 - success,1 - failure
//**************************************************************************************
int ExeCmd(Smash* smash, char* lineSize, char* cmdString)
{
	char* cmd; 
	char* args[MAX_ARG];
	char pwd[MAX_PATH_SIZE];
	char const *delimiters = " \t\n";
	int i = 0, num_arg = 0;
	bool illegal_cmd = false; // illegal command
    	cmd = strtok(lineSize, delimiters);
	if (cmd == nullptr)
		return 0; 
   	args[0] = cmd;
	for (i=1; i<MAX_ARG; i++)
	{
		args[i] = strtok(nullptr, delimiters);
		if (args[i] != nullptr)
			num_arg++; 
 
	}

	// insert new cmd to history array
	smash->add_new_cmd_to_hist_list(cmdString);


/*************************************************/
// Built in Commands PLEASE NOTE NOT ALL REQUIRED
// ARE IN THIS CHAIN OF IF COMMANDS. PLEASE ADD
// MORE IF STATEMENTS AS REQUIRED
/*************************************************/
    if (!strcmp(cmd, "cd") ) {
        char* target=args[1];
        if (num_arg == 1) {
            if (!strcmp(args[1], "-")) {
                char n_pwd[MAX_PATH_SIZE];
                strcpy(n_pwd, l_pwd);
                target=n_pwd;
            }

            getcwd(l_pwd, MAX_PATH_SIZE); // updating l_pwd
            if (chdir(target)) {
                printf("smash error: > \"%s\" - path not found\n", cmdString);
                perror("smash error: >");
                return 1;// fail
            }
        } else { //too many/few arguments
            illegal_cmd = true;
        }
    }
	
	/*************************************************/
	else if (!strcmp(cmd, "pwd")) 
	{
		if (num_arg != 0) {
            illegal_cmd = true;
		} else {
            getcwd(pwd, MAX_PATH_SIZE);
            printf("%s\n", pwd);
		}
	}
	
	/*************************************************/
	else if (!strcmp(cmd, "mv"))
	{
        if (num_arg != 2) {
            illegal_cmd = true;
        } else {
            int res = rename(args[1], args[2]);
            if (res == 0) {
                cout << args[1] << " has been renamed to " << args[2] << endl;
            } else {
                perror("smash error: >");
            }
        }
	}
	/*************************************************/
	
	else if (!strcmp(cmd, "jobs"))
	{
        if (num_arg > 0)
            illegal_cmd = true;
        else
            smash->print_job_list();
	}
	/*************************************************/
    else if (!strcmp(cmd, "showpid"))
    {
        if (num_arg > 0) {
            illegal_cmd = true;
        } else {
            pid_t pid = getpid();
            printf("smash pid is %ld\n", (long)pid);
        }
    }
	/*************************************************/
	else if (!strcmp(cmd, "fg")) 
	{
        int job_id = NO_JOB_ID;
	    if (num_arg == 0) {

            if (smash->last_bg_job != NO_JOB_ID) {
                job_id = smash->last_bg_job;
            } else {
                printf("smash error: > fg - no last bg command\n");
                return 1;  // fail
            }

        } else if (num_arg == 1) {
            job_id = std::stoi(args[1]);
	    } else {
            illegal_cmd = true;
	    }

	    if (!illegal_cmd) {

	        Job *job = smash->get_job_from_list(job_id);
            if (job == nullptr) {
                printf("smash error: > fg - job does not exist in job list\n");
                return 1; // fail
            }

            job->set_job_state_running();  // if this didn't succeeed, he will change status and we will catch it and set to the correct state
            cout << job->name_of_cmd << "\n";
            kill(job->pid, SIGCONT);
            std::cout<<"signal SIGCONT was sent to pid "<<job->pid<<std::endl;


            int res_status;
            smash->fg_pid=job->pid;
            pid_t ret_pid = waitpid(job->pid, &res_status, WUNTRACED);
            smash->fg_pid=SMASH_IS_RUNNING;
            if (ret_pid > 0){
                if (WIFEXITED(res_status) || WIFSIGNALED(res_status)) { // child exited -> make sure he is not in the jobs list anymore
                    smash->remove_job_in_list_via_pID(ret_pid);
                } else if (WIFSTOPPED(res_status)) {  // child stopped -> make sure he is in the jobs list

                    // find if pid has a job already, if yes update, if no create a new one for it
                    Job* job = smash->get_job_from_list_via_pID(ret_pid);
                    if (job != nullptr) {
                        smash->update_job_stopped_in_list_via_pID(ret_pid);
                    } else {
                        smash->create_new_job_in_list(ret_pid, cmdString);
                        smash->update_job_stopped_in_list_via_pID(ret_pid);
                    }
                    smash->last_suspended_job=smash->get_job_via_pid(ret_pid);

                } else {  // we are not supposed to handle any other statuses
                    ;
                }
            } else {
                ;  // if there was an error, the child fork will print it
            }
        }
    }
	/*************************************************/
	else if (!strcmp(cmd, "bg")) 
	{
        int job_id = NO_JOB_ID;
        if (num_arg == 0) {

	        if (smash->last_suspended_job != NO_JOB_ID) {
                job_id = smash->last_suspended_job;
	        } else {
                printf("smash error: > bg - no last suspended command\n");
                return 1;  // fail
	        }

        } else if (num_arg == 1){
	        job_id = std::stoi(args[1]);
        } else {
            illegal_cmd = true;
        }

        if (!illegal_cmd) {

            Job *job = smash->get_job_from_list(job_id);
            if (job == nullptr) {
                printf("smash error: > bg - job does not exist in job list\n");
                return 1; // fail
            }

            kill(job->pid, SIGCONT);
            std::cout<<"signal SIGCONT was sent to pid "<<job->pid<<std::endl;
            job->set_job_state_running();  // if this didn't succeeed, he will change status and we will catch it and set to the correct state
            smash->last_bg_job = job_id;
            cout << job->name_of_cmd << "\n";
        }
	}
	/*************************************************/
	else if (!strcmp(cmd, "quit"))
	{

	    if (num_arg == 1) {
	        if (strcmp(args[1], "kill") != 0) {
	            illegal_cmd = true;
	        } else {
	            smash->kill_all_jobs();
	        }
	    } else if (num_arg == 0) {
            ;  // ok input, but nothing to process
	    } else {
            illegal_cmd = true;
	    }

        if (!illegal_cmd) {
            smash->is_quit = true;
        }

	} 
	/*************************************************/
    else if (!strcmp(cmd, "history"))
    {
        if (num_arg != 0) {
            illegal_cmd = true;
        } else {

            smash->print_hist_list();

        }

    }

    else if (!strcmp(cmd, "kill"))
    {
        if (num_arg != 2||args[1][0]!='-'||strlen(args[1])<2)
            illegal_cmd = true;
        else {
            try {
                int job_id = std::stoi(args[2]);
                int signal_num = std::stoi(&args[1][1]);
                Job* job=smash->get_job_from_list(job_id);
                if (job== nullptr){
                    printf("smash error: > kill job - job does not exist\n"); //job not exist
                    return 1;
                }
                else if(kill(job->pid,signal_num)){
                    printf("smash error: > kill job - cannot send signal\n"); //kill fail
                    return 1;
                }
                std::cout<<"signal "<<signal_names[signal_num-1] << " was sent to pid "<<job->pid<<std::endl;



            }
            catch (std::logic_error &e){
                illegal_cmd = true;
            }


            //if(smash->get_job_from_list((int)args[2])
        }
    }

        /*************************************************/
	else // external command
	{
 		ExeExternal(smash, args, cmdString);
	 	return 0;
	}
	if (illegal_cmd == true)
	{
		printf("smash error: > \"%s\"\n", cmdString);
		return 1;
	}
    return 0;
}
//**************************************************************************************
// function name: ExeExternal
// Description: executes external command
// Parameters: external command arguments, external command string
// Returns: void
//**************************************************************************************
void ExeExternal(Smash* smash, char *args[MAX_ARG], char* cmdString)
{
	int pID;
    	switch(pID = fork()) 
	{
        case -1:{//  (error)
            perror("smash:");
            exit(1);
        }

        case 0 : {
            // Child Process
            setpgrp();
            execv(args[0], args);
            //if we got here execv failed
            perror("smash");
            exit(1);
        }

        default:
            int res_status;
            smash->fg_pid=pID;
            pid_t ret_pid = waitpid(pID, &res_status, WUNTRACED);
            smash->fg_pid=SMASH_IS_RUNNING;
            if (ret_pid> 0){
                if (WIFEXITED(res_status) || WIFSIGNALED(res_status)) { // child exited -> make sure he is not in the jobs list anymore
                    smash->remove_job_in_list_via_pID(ret_pid);
                } else if (WIFSTOPPED(res_status)) {  // child stopped -> make sure he is in the jobs list

                    // find if pid has a job already, if yes update, if no create a new one for it
                    Job* job = smash->get_job_from_list_via_pID(ret_pid);
                    if (job != nullptr) {
                        smash->update_job_stopped_in_list_via_pID(ret_pid);
                    } else {
                        smash->create_new_job_in_list(ret_pid, cmdString);
                        smash->update_job_stopped_in_list_via_pID(ret_pid);
                    }
                    smash->last_suspended_job=smash->get_job_via_pid(ret_pid);

                } else {  // we are not supposed to handle any other statuses
                    ;
                }
            } else {
                ;  // if there was an error, the child fork will print it
            }
        }
    }
//**************************************************************************************
// function name: ExeComp
// Description: executes complicated command
// Parameters: command string
// Returns: 0- if complicated -1- if not
//**************************************************************************************
int ExeComp(char* lineSize)
{
    if ((strstr(lineSize, "|")) || (strstr(lineSize, "<")) || (strstr(lineSize, ">")) || (strstr(lineSize, "*")) || (strstr(lineSize, "?")) || (strstr(lineSize, ">>")) || (strstr(lineSize, "|&")))
    {
		// Add your code here (execute a complicated command)
					
		/* 
		your code
		*/
        return  0;
	} 
	return -1;
}
//**************************************************************************************
// function name: BgCmd
// Description: if command is in background, insert the command to jobs
// Parameters: command string, pointer to smash
// Returns: 0- BG command -1- if not
//**************************************************************************************
int BgCmd(char* lineSize,Smash* smash)
{

	char* cmd;
	char const *delimiters = " \t\n";
	char *args[MAX_ARG];
	if (lineSize[strlen(lineSize)-2] == '&')
	{
		lineSize[strlen(lineSize)-2] = '\0';

		int i = 0, num_arg = 0;
        cmd = strtok(lineSize, delimiters);
        if (cmd == nullptr)
            return 0;
        args[0] = cmd;
        for (i=1; i<MAX_ARG; i++)
        {
            args[i] = strtok(nullptr, delimiters);
            if (args[i] != nullptr)
                num_arg++;

        }

        int pID;
        switch(pID = fork()) {
            case -1:{//  (error)
                perror("smash:");
                exit(1);
             }

            case 0 : {
                // Child Process
                setpgrp();
                execv(cmd, args);
                //if we got here execv failed
                perror("smash");
                exit(1);
            }

            default:
                int job_id = smash->create_new_job_in_list(pID,cmd);
                smash->last_bg_job = job_id;
                return 0;
        }
		
	}
	return -1;
}

