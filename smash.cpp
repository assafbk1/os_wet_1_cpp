/*	smash.c
main file. This file contains the main function of smash
*******************************************************************/
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include "commands.h"
#include "signals.h"
#include <string>
#include <ctime>
#include <time.h>
#include <iostream>
#include <list>
#include "smash_class.h"


#define MAX_LINE_SIZE 80
#define MAXARGS 20
#define INITIAL_JOB_ID 1
#define NO_JOB_ID -1  // this means that no job matches the wanted value


using std::string;
using std::time_t;
using std::cout;
using std::list;

Smash smash;
char* L_Fg_Cmd;
char lineSize[MAX_LINE_SIZE];


//**************************************************************************************
// function name: main
// Description: main function of smash. get command from user and calls command functions
//**************************************************************************************
int main(int argc, char *argv[])
{

        char cmdString[MAX_LINE_SIZE];

        //signal declaretions
        //NOTE: the signal handlers and the function/s that sets the handler should be found in siganls.c
        // add your code here
        void sigint_handler(int sig_num);
        void sigtstp_handler(int sig_num);
        //########################################
        //NOTE: the signal handlers and the function/s that sets the handler should be found in siganls.c
        //set your signal handlers here
        // add your code here

        //########################################

        //########################################
        // Init globals

        L_Fg_Cmd = (char *) malloc(sizeof(char) * (MAX_LINE_SIZE + 1));
        if (L_Fg_Cmd == nullptr)
            exit(-1);
        L_Fg_Cmd[0] = '\0';
        signal(SIGINT,&sigint_handler);
        signal(SIGTSTP,&sigtstp_handler);


        while (1) {
            printf("smash > ");
            fgets(lineSize, MAX_LINE_SIZE, stdin);
            strcpy(cmdString, lineSize);
            cmdString[strlen(lineSize) - 1] = '\0';

            int ret_status;
            pid_t pID_terminated = waitpid(-1, &ret_status, WNOHANG | WUNTRACED | WCONTINUED);
            while(pID_terminated > 0){
                if (WIFEXITED(ret_status) || WIFSIGNALED(ret_status)) { // child exited
                    smash.remove_job_in_list_via_pID(pID_terminated);
                } else if (WIFSTOPPED(ret_status)) {  // child stopped
                    smash.update_job_stopped_in_list_via_pID(pID_terminated);
                    smash.last_suspended_job=smash.get_job_via_pid(pID_terminated);
                } else if (WIFCONTINUED(ret_status)) {  // child continued
                    smash.update_job_continued_in_list_via_pID(pID_terminated);
                } else {  // we are not supposed to handle any other statuses
                    ;
                }
                pID_terminated = waitpid(-1, &ret_status, WNOHANG | WUNTRACED | WCONTINUED);
            }


            // perform a complicated Command
            if (!ExeComp(lineSize)) continue;
            // background command
            if (!BgCmd(lineSize, &smash)) continue;
            // built in commands
            ExeCmd(&smash, lineSize, cmdString);


            pID_terminated = waitpid(-1, &ret_status, WNOHANG | WUNTRACED | WCONTINUED);
            while(pID_terminated > 0){
                if (WIFEXITED(ret_status) || WIFSIGNALED(ret_status)) { // child exited
                    smash.remove_job_in_list_via_pID(pID_terminated);
                } else if (WIFSTOPPED(ret_status)) {  // child stopped
                    smash.update_job_stopped_in_list_via_pID(pID_terminated);
                    smash.last_suspended_job=smash.get_job_via_pid(pID_terminated);
                } else if (WIFCONTINUED(ret_status)) {  // child continued
                    smash.update_job_continued_in_list_via_pID(pID_terminated);
                } else {  // we are not supposed to handle any other statuses
                    ;
                }
                pID_terminated = waitpid(-1, &ret_status, WNOHANG | WUNTRACED | WCONTINUED);
            }

            // initialize for next line read
            lineSize[0] = '\0';
            cmdString[0] = '\0';

            if (smash.is_quit) {
                break;
            }
    }
    return 0;
}

