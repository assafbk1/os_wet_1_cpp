//
// Created by os on 29.11.2019.
//

#ifndef OS_WET_1_CPP_SMASH_CLASS_H
#define OS_WET_1_CPP_SMASH_CLASS_H
#include <string>
#include <ctime>
#include <time.h>
#include <iostream>
#include <list>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
using std::string;
using std::time_t;
using std::cout;
using std::list;

#define INITIAL_JOB_ID 1
#define NO_JOB_ID -1  // this means that no job matches the wanted value
#define NO_PID -1     // this means that no pid matches the wanted value
#define HISTORY_CMD "history"
#define SMASH_IS_RUNNING 0
#define TIME_TO_WAIT_FOR_SIGTERM 5

class Job {

public:
    int job_id;
    pid_t pid;
    string name_of_cmd;
    time_t time_entered_job_list;
    bool is_running;

    Job(int job_id, pid_t pid, string name_of_cmd): job_id(job_id), pid(pid), name_of_cmd(std::move(name_of_cmd)),time_entered_job_list(time(
            nullptr)), is_running(
            true) {

      //  time(&time_entered_job_list);

    }


    void set_job_state_running() {
        is_running = true;
    }
    void set_job_state_stopped() {
        is_running = false;
    }

    void print_job() {

        string is_running_and_end_of_line_data;
        if (!is_running) {
            is_running_and_end_of_line_data = " (stopped)\n";
        } else {
            is_running_and_end_of_line_data = "\n";
        }

        time_t cur_time = time(nullptr);
        long int time_lived = difftime(cur_time, time_entered_job_list);

        cout << "[" << job_id << "] " << name_of_cmd << ": " << pid << " " <<  time_lived << " secs" << is_running_and_end_of_line_data;

    }
};

class Smash {
public:

    list<Job> job_list;
    int next_job_id;  // holds the next available job id
    int last_suspended_job;  // holds the pid of the last suspended job
    int last_bg_job;  // holds the pid of the last job put to bg
    list<string> hist_list;
    int hist_cmd_cnt;

    bool is_quit;
    pid_t fg_pid;   //holds the pid of the process that currently running.
    
    Smash(): next_job_id(INITIAL_JOB_ID), last_suspended_job(NO_JOB_ID), last_bg_job(NO_JOB_ID), hist_cmd_cnt(0), is_quit(false),fg_pid(SMASH_IS_RUNNING) {

    }

    int create_new_job_in_list(pid_t pid,string cmd){
        Job new_job(next_job_id++,pid, std::move(cmd));
        job_list.push_back(new_job);
        return next_job_id-1;
    }

    void remove_job_in_list_via_job_id(int job_id){
        for (std::list<Job>::iterator it=job_list.begin(); it != job_list.end(); ++it){
            if(it->job_id==job_id){
                job_list.erase(it);
                break;
            }
        }
    }

    void remove_job_in_list_via_pID(pid_t pid){
        for (std::list<Job>::iterator it=job_list.begin(); it != job_list.end(); ++it){
            if(it->pid==pid){
                job_list.erase(it);
                break;
            }
        }
    }

    void update_job_stopped_in_list_via_pID(pid_t pid){
        for (std::list<Job>::iterator it=job_list.begin(); it != job_list.end(); ++it){
            if(it->pid==pid){
                it->set_job_state_stopped();
                break;
            }
        }
    }

    void update_job_continued_in_list_via_pID(pid_t pid){
        for (std::list<Job>::iterator it=job_list.begin(); it != job_list.end(); ++it){
            if(it->pid==pid){
                it->set_job_state_running();
                break;
            }
        }
    }

    Job* get_job_from_list(int job_id) {
        for (std::list<Job>::iterator it=job_list.begin(); it != job_list.end(); ++it){
            if(it->job_id==job_id){
                return &(*it);
            }
        }
        return nullptr;
    }

    int get_job_via_pid(pid_t pid) {
        for (std::list<Job>::iterator it=job_list.begin(); it != job_list.end(); ++it){
            if(it->pid==pid){
                return it->job_id;
            }
        }
        return NO_JOB_ID;
    }

    Job* get_job_from_list_via_pID(pid_t pID) {
        for (std::list<Job>::iterator it=job_list.begin(); it != job_list.end(); ++it){
            if(it->pid==pID){
                return &(*it);
            }
        }
        return nullptr;
    }

    void kill_all_jobs() {

        int ret_status;
        time_t start_time, cur_time;
        for (std::list<Job>::iterator it=job_list.begin(); it != job_list.end(); ++it){
            cout << "[" << it->job_id << "] " << it->name_of_cmd << "  – Sending SIGTERM... ";
            kill(it->pid, SIGTERM);

            pid_t pID_terminated = waitpid(it->pid, &ret_status, WNOHANG);
            time(&start_time);
            time(&cur_time);
            while (pID_terminated <= 0 && cur_time-start_time < TIME_TO_WAIT_FOR_SIGTERM) {
                pID_terminated = waitpid(it->pid, &ret_status, WNOHANG);
                time(&cur_time);
            }

            // if we entered this cond it means that 5 seconds passed and the process was still not terminated
            if (cur_time-start_time >= TIME_TO_WAIT_FOR_SIGTERM) {
                cout << "(5 sec passed) Sending SIGKILL… ";
                kill(it->pid, SIGKILL);
            }

            cout << "Done.\n";
        }

    }

    void print_job_list(){
        for (std::list<Job>::iterator it=job_list.begin(); it != job_list.end(); ++it){
            it->print_job();
        }
    };

    void add_new_cmd_to_hist_list(string cmd) {

        if((cmd.compare(HISTORY_CMD)) == 0)  // we don't add history to the last commands
        {
            return;
        }

        if (hist_cmd_cnt < 50) {
            hist_cmd_cnt++;
            hist_list.push_back(cmd);
        } else {
            hist_list.pop_front();
            hist_list.push_back(cmd);
        }

    }

    void print_hist_list(){
        for (std::list<string>::iterator it=hist_list.begin(); it != hist_list.end(); ++it){
            cout << *it << std::endl;
        }
    };


};


#endif //OS_WET_1_CPP_SMASH_CLASS_H
