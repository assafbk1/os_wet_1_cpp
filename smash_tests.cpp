
#include "smash_tests.h"
#define DEMO_PATH "/home/os/Desktop/demo&\n"

void init_simple_command(char* lineSize, char* cmdString) {
    strcpy(cmdString, lineSize);
    cmdString[strlen(lineSize)-1]='\0';
}


void test_pwd_1() {

    char lineSize[MAX_LINE_SIZE] = "pwd\n";
    char cmdString[MAX_LINE_SIZE];
    init_simple_command(lineSize, cmdString);
    Smash smash = Smash();

    int res = ExeCmd(&smash, lineSize, cmdString);
    assert(res==0);

}

void test_pwd_2() {

    char lineSize[MAX_LINE_SIZE] = "pwd 1\n";
    char cmdString[MAX_LINE_SIZE];
    init_simple_command(lineSize, cmdString);
    Smash smash = Smash();

    int res = ExeCmd(&smash, lineSize, cmdString);
    assert(res==1);

}

void test_cd() {

    char lineSize[MAX_LINE_SIZE] = "cd \n";// suppose to fail to few arguments
    char cmdString[MAX_LINE_SIZE];
    char pwd[MAX_LINE_SIZE];
    char Lpwd[MAX_LINE_SIZE];
    init_simple_command(lineSize, cmdString);
    Smash smash = Smash();

    int res = ExeCmd(&smash, lineSize, cmdString);
    assert(res==1);

    strcpy(lineSize,"cd /home 12");// suppose to fail too many arguments
    init_simple_command(lineSize, cmdString);
    res = ExeCmd(&smash, lineSize, cmdString);
    assert(res==1);

    strcpy(lineSize,"cd /home12");// suppose to fail path not exist
    init_simple_command(lineSize, cmdString);
    res = ExeCmd(&smash, lineSize, cmdString);
    assert(res==1);

    getcwd(Lpwd, MAX_LINE_SIZE);
    strcpy(lineSize,"cd /home/os");// suppose to succeed
    init_simple_command(lineSize, cmdString);
    res = ExeCmd(&smash, lineSize, cmdString);
    assert(res==0);
    getcwd(pwd, MAX_LINE_SIZE);
    assert(!strcmp(pwd,"/home/os"));

    strcpy(lineSize,"cd -");// suppose to succeed and return us to the last location
    init_simple_command(lineSize, cmdString);
    res = ExeCmd(&smash, lineSize, cmdString);
    assert(res==0);
    getcwd(pwd, MAX_LINE_SIZE);
    assert(!strcmp(pwd,Lpwd));

    strcpy(lineSize,"cd -");// suppose to succeed and return us to the last location
    init_simple_command(lineSize, cmdString);
    res = ExeCmd(&smash, lineSize, cmdString);
    assert(res==0);
    getcwd(pwd, MAX_LINE_SIZE);
    assert(!strcmp(pwd,"/home/os"));

    strcpy(lineSize,"cd -");// suppose to succeed and return us to the last location
    init_simple_command(lineSize, cmdString);
    res = ExeCmd(&smash, lineSize, cmdString);
    assert(res==0);
    getcwd(pwd, MAX_LINE_SIZE);
    assert(!strcmp(pwd,Lpwd));

}

void test_showpid(){
    char lineSize[MAX_LINE_SIZE] = "showpid 1\n"; //too many arguments
    char cmdString[MAX_LINE_SIZE];
    init_simple_command(lineSize, cmdString);
    Smash smash = Smash();

    int res = ExeCmd(&smash, lineSize, cmdString);
    assert(res==1);

    strcpy(lineSize, "showpid");
    init_simple_command(lineSize, cmdString);
    res = ExeCmd(&smash, lineSize, cmdString);
    assert(res==0);

}

void test_history_list_1() {

    Smash smash = Smash();

    string cmd_1 = "ls";
    string cmd_2 = "history";
    string cmd_3 = "cd";

    smash.add_new_cmd_to_hist_list(cmd_1);
    smash.add_new_cmd_to_hist_list(cmd_2);
    smash.add_new_cmd_to_hist_list(cmd_3);

    assert(smash.hist_list.front().compare(cmd_1) == 0);
    assert(smash.hist_list.back().compare(cmd_3) == 0);
    assert(smash.hist_list.size()==2);

}

void test_history_list_2() {

    Smash smash = Smash();

    string cmd = "command number ";

    for (int i=0; i < 55; i++) {
        smash.add_new_cmd_to_hist_list(cmd + std::to_string(i));
    }

    assert(smash.hist_list.size()==50);

    // we start from command 5 [expecting commands 0 to 4 are gone]
    int cnt = 5;
    string expected;
    for (std::list<string>::iterator it=smash.hist_list.begin(); it != smash.hist_list.end(); ++it){
        expected = cmd + std::to_string(cnt);
        assert((*it) == expected);
        cnt = (cnt+1);

    }

    assert(cnt == 55); // if we are at 55 this means that we iterated over 50 strings


}

void test_print_history_list() {

    Smash smash = Smash();

    string cmd = "command number ";

    for (int i=0; i < 3; i++) {
        smash.add_new_cmd_to_hist_list(cmd + std::to_string(i));
    }

    smash.print_hist_list();

}

void test_history() {

    Smash smash = Smash();

    char lineSize[MAX_LINE_SIZE] = "history\n";
    char cmdString[MAX_LINE_SIZE];
    init_simple_command(lineSize, cmdString);
    ExeCmd(&smash, lineSize, cmdString);

    strcpy(lineSize, "#pwd\n");
    init_simple_command(lineSize, cmdString);
    ExeCmd(&smash, lineSize, cmdString);

    strcpy(lineSize, "ls\n");
    init_simple_command(lineSize, cmdString);
    ExeCmd(&smash, lineSize, cmdString);

    strcpy(lineSize, "ls -l -x\n");
    init_simple_command(lineSize, cmdString);
    ExeCmd(&smash, lineSize, cmdString);

    strcpy(lineSize, "history\n");
    init_simple_command(lineSize, cmdString);
    ExeCmd(&smash, lineSize, cmdString);



}

void test_bg_fail_1() {

    Smash smash = Smash();

    char lineSize[MAX_LINE_SIZE] = "bg\n";
    char cmdString[MAX_LINE_SIZE];
    init_simple_command(lineSize, cmdString);
    ExeCmd(&smash, lineSize, cmdString);

}

void test_bg_fail_2() {

    Smash smash = Smash();

    pid_t pid_1 = 123;
    pid_t pid_2 = 124;
    smash.create_new_job_in_list(pid_1, "ls");
    smash.create_new_job_in_list(pid_2, "ls -l");

    char lineSize[MAX_LINE_SIZE] = "bg 3\n";
    char cmdString[MAX_LINE_SIZE];
    init_simple_command(lineSize, cmdString);
    ExeCmd(&smash, lineSize, cmdString);

}

//void test_bg_1() {
//
//    Smash smash = Smash();
//
//    smash.create_new_job_in_list("ls");
//    smash.create_new_job_in_list("ls -l");
//
//    char lineSize[MAX_LINE_SIZE] = "bg 3\n";
//    char cmdString[MAX_LINE_SIZE];
//    init_simple_command(lineSize, cmdString);
//    ExeCmd(&smash, lineSize, cmdString);
//
//}

void test_kill() {
    cout<<"kill_tests"<<std::endl;

    char lineSize[MAX_LINE_SIZE] = "kill -55 8 j \n";// suppose to fail to many arguments
    char cmdString[MAX_LINE_SIZE];
    init_simple_command(lineSize, cmdString);
    Smash smash = Smash();

    int res = ExeCmd(&smash, lineSize, cmdString);
    assert(res==1);

    strcpy(lineSize,"kill 9 0 12");// suppose to fail no '-' in signal
    init_simple_command(lineSize, cmdString);
    res = ExeCmd(&smash, lineSize, cmdString);
    assert(res==1);

    strcpy(lineSize,"kill -9 8");// suppose to fail job not exist
    init_simple_command(lineSize, cmdString);
    res = ExeCmd(&smash, lineSize, cmdString);
    assert(res==1);

    strcpy(lineSize,DEMO_PATH);// creating a new bg process
    res = BgCmd(lineSize, &smash);

    assert(res==0);

    strcpy(lineSize,"kill -100 1");// suppose to fail signal not exist
    init_simple_command(lineSize, cmdString);
    res = ExeCmd(&smash, lineSize, cmdString);
    assert(res==1);


    strcpy(lineSize,"kill -9 1");// suppose to succeed and terminat job;
    init_simple_command(lineSize, cmdString);
    res = ExeCmd(&smash, lineSize, cmdString);
    assert(res==0);
}



void run_smash_tests() {
    test_pwd_1();
    test_pwd_2();
    test_cd();
    test_showpid();
    test_history_list_1();
    test_history_list_2();
//    test_print_history_list();
    test_history();
    test_bg_fail_1();
    test_bg_fail_2();
    test_kill();
    printf("all tests passed\n");

}