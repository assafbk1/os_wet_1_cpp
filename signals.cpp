// signals.c
// contains signal handler funtions
// contains the function/s that set the signal handlers

/*******************************************/
/* Name: handler_cntlc
   Synopsis: handle the Control-C */
#include "signals.h"


void sigint_handler(int sig_num){
    if(smash.fg_pid>0){
        kill (smash.fg_pid,SIGINT);
        std::cout<<"signal SIGINT was sent to pid "<<smash.fg_pid<<std::endl;
    }
}


void sigtstp_handler(int sig_num){
    if(smash.fg_pid>0){
        kill(smash.fg_pid, SIGTSTP);
        std::cout<<"signal SIGTSTP was sent to pid "<<smash.fg_pid<<std::endl;
    }

}


